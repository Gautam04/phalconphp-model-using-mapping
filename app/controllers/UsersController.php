<?php
// WE DEFINED THESE NAMESPACES IN LOADER. WE ARE NOW WORKING WITH USERCONTROLLER
namespace App\Controllers;

// ALL THE BELOW PHALCON LIBRARY CLASSES WILL BE USED.
// ALSO DEFINE THE SERVICES THAT WILL WORK WITH THIS CONTROLLER
use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http422Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\AbstractService;
use App\Services\ServiceException;
use App\Services\UsersService;


class UsersController extends AbstractController
{
 


    public function addAction()
    {
	   
		$data = $this->request->getJsonRawBody(true);

        try 
        {
            $this->usersService->createUser($data);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case AbstractService::ERROR_ALREADY_EXISTS:
                case UsersService::ERROR_UNABLE_CREATE_USER:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

    
    public function getUserListAction()
    {
        try 
        {
            $this->logger->log('this is a message');
            $userList = $this->usersService->getUserList();
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userList;
        
    }

    public function getSingleUserListAction($userId)
    {
        try 
        {
            $this->logger->log('this is a message');
            $userData = $this->usersService->getSingleUser($userId);
        
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userData;
        
    }

    public function getUserDepartmentListAction()
    {
        try 
        {
            $this->logger->log('this is a message');
            $userData = $this->usersService->getDeptList();
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userData;
        
    }

    public function getUserProjectListAction()
    {
        try 
        {
            $this->logger->log('this is a message');
            $userData = $this->usersService->getProjectList();
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userData;
        
    }

     /**
     * UPDATING EXISTING USER
     *
     * @param string $userId
     */
    public function updateUserAction($userId)
    {
        $errors = [];
        $data   = [];

		$data = $this->request->getJsonRawBody(true);

        $data['id'] = (int)$userId;

        if ($errors) 
        {
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        try 
        {
           $this->usersService->updateUser($data);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case UsersService::ERROR_UNABLE_UPDATE_USER:
                case UsersService::ERROR_USER_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

    /**
     * DELETE AN EXISTING USER
     *
     * @param string $userId
     */
    public function deleteUserAction($userId)
    {
        if (!ctype_digit($userId) || ($userId < 0)) 
        {
            $errors['userId'] = 'Id must be a positive integer';
        }

        try 
        {
            $this->usersService->deleteUser((int)$userId);

        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case UsersService::ERROR_UNABLE_DELETE_USER:
                case UsersService::ERROR_USER_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception($e->getMessage());
            }
        }
    }
}
