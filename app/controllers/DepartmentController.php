<?php
// WE DEFINED THESE NAMESPACES IN LOADER. WE ARE NOW WORKING WITH USERCONTROLLER
namespace App\Controllers;

// ALL THE BELOW PHALCON LIBRARY CLASSES WILL BE USED.
// ALSO DEFINE THE SERVICES THAT WILL WORK WITH THIS CONTROLLER
use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http422Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\AbstractService;
use App\Services\ServiceException;
use App\Services\DepartmentService;


class DepartmentController extends AbstractController
{
 


    public function addDeptAction()
    {
	   
		$data = $this->request->getJsonRawBody(true);

        try 
        {
            $this->departmentService->createDept($data);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case AbstractService::ERROR_ALREADY_EXISTS:
                case DepartmentService::ERROR_UNABLE_CREATE_DEPARTMENT:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

    
    public function getDeptListAction()
    {
        try 
        {
            $this->logger->log('this is a message');
            $deptList = $this->departmentService->getDeptList();
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $deptList;
        
    }

    public function getSingleDeptListAction($userId)
    {
        try 
        {
            $this->logger->log('this is a message');
            $deptData = $this->departmentService->getSingleDept($userId);
        
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $deptData;
        
    }

    public function updateDeptAction($deptId)
    {
        $errors = [];
        $data   = [];

		$data = $this->request->getJsonRawBody(true);

        $data['id'] = (int)$deptId;

        if ($errors) 
        {
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        try 
        {
            $this->departmentService->updateDept($data);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case DepartmentService::ERROR_UNABLE_UPDATE_DEPARTMENT:
                case DepartmentService::ERROR_DEPARTMENT_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

    /**
     * DELETE AN EXISTING USER
     *
     * @param string $userId
     */
    public function deleteDeptAction($deptId)
    {
        if (!ctype_digit($deptId) || ($deptId < 0)) 
        {
            $errors['deptId'] = 'Id must be a positive integer';
        }

        try 
        {
            $this->departmentService->deleteDept((int)$deptId);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case DepartmentService::ERROR_UNABLE_DELETE_DEPARTMENT:
                case DepartmentService::ERROR_DEPARTMENT_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }
}
