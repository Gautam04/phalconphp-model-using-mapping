<?php
// WE DEFINED THESE NAMESPACES IN LOADER. WE ARE NOW WORKING WITH ADDRESSCONTROLLER
namespace App\Controllers;

use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http422Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\AbstractService;
use App\Services\ServiceException;
use App\Services\AddressService;

/**
 * Operations with ADDRESSs: CRUD
 */
class AddressController extends AbstractController
{
    public function addAddressAction()
    {
		$data = $this->request->getJsonRawBody(true);
	
        try 
        {
            $this->addressService->createAddress($data);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case AbstractService::ERROR_ALREADY_EXISTS:
                case AddressService::ERROR_UNABLE_CREATE_ADDRESS:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

  
    public function getAddressListAction()
    {
        try 
        {
            $this->logger->log('this is a message');
            $addressList = $this->addressService->getAddressList();
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $addressList;
        
    }

    public function getSingleAddressAction($addId)
    {
        try 
        {
            $this->logger->log('this is a message');
            $addressData = $this->addressService->getSingleAddress($addId);
        
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $addressData;
        
    }


    public function updateAddressAction($addId)
    {
        $errors = [];
        $data   = [];

		$data = $this->request->getJsonRawBody(true);

        $data['id'] = (int)$addId;

        if ($errors) 
        {
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        try 
        {
            $this->addressService->updateAddress($data);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case AddressService::ERROR_UNABLE_UPDATE_ADDRESS:
                case AddressService::ERROR_ADDRESS_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

    public function deleteAddressAction($addId)
    {
        if (!ctype_digit($addId) || ($addId < 0)) 
        {
            $errors['addId'] = 'Id must be a positive integer';
        }

        try 
        {
            $this->addressService->deleteAddress((int)$addId);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case AddressService::ERROR_UNABLE_DELETE_ADDRESS:
                case AddressService::ERROR_ADDRESS_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }
}
