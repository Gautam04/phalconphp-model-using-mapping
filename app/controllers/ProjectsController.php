<?php
// WE DEFINED THESE NAMESPACES IN LOADER. WE ARE NOW WORKING WITH USERCONTROLLER
namespace App\Controllers;

// ALL THE BELOW PHALCON LIBRARY CLASSES WILL BE USED.
// ALSO DEFINE THE SERVICES THAT WILL WORK WITH THIS CONTROLLER
use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http422Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\AbstractService;
use App\Services\ServiceException;
use App\Services\ProjectsService;

/**
 * Operations with Users: CRUD
 */
class ProjectsController extends AbstractController
{
    // THIS FUNCTION ADDS A NEW USER TO THE USER TABLE
    // THE CODE BELOW IS A SAMPLE OF HOW TO USE THE CONTROLLER WITH SERVICE AND MODEL

    public function addProjectAction()
    {
	    /** INIT BLOCK **/
        
       
	    /** END INIT BLOCK **/

	    /** VALIDATION BLOCK **/
		$data = $this->request->getJsonRawBody(true);
		
       
        

        // IF ERRORS FOUND IN VALIDATION, THROW EXCEPTION
       
        
	    /** END VALIDATION BLOCK **/

	    // PASSING TO BUSINESS LOGIC AND PREPARING THE RESPONSE. CREATE THE USER IF VALIDATION PASSED
        try 
        {
            $this->projectsService->createProject($data);
        

        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case AbstractService::ERROR_ALREADY_EXISTS:
                case ProjectsService::ERROR_UNABLE_CREATE_PROJECT:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
	    /** END PASSING TO BUSINESS LOGIC AND PREPARING THE RESPONSE  **/
    }

    /**
     * RETURNS USER LIST
     *
     * @return array
     */
    public function getProjectListAction()
    {
        try 
        {
            // $this->logger->log('this is a message');
            $userList = $this->projectsService->getProjectList();
        } 
        catch (ServiceException $e) 
        {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userList;
        
    }

    // public function getSingleUserListAction($userId)
    // {
    //     try 
    //     {
    //         // $this->logger->log('this is a message');
    //         $userData = $this->usersService->getSingleUser($userId);
    //     } 
    //     catch (ServiceException $e) 
    //     {
    //         throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
    //     }

    //     return $userData;
        
    // }

     /**
     * UPDATING EXISTING USER
     *
     * @param string $userId
     */
    // public function updateUserAction($userId)
    // {
    //     $errors = [];
    //     $data   = [];

    //     $data['login'] = $this->request->getPut('login');
    //     if ((!is_null($data['login'])) && (!is_string($data['login']) || !preg_match (
    //                 '/^[A-z0-9_-]{3,16}$/',
    //           $data['login']
    //         ))
    //     ) 
    //     {
    //         $errors['login'] = 'Login must consist of 3-16 latin symbols, numbers or \'-\' and \'_\' symbols';
    //     }

    //     $data['password'] = $this->request->getPut('password');
    //     if ((!is_null($data['password'])) && (!is_string($data['password']) || !preg_match (
    //                 '/^[A-z0-9_-]{6,18}$/',
    //           $data['password']
    //         ))
    //     ) 
    //     {
    //         $errors['password'] = 'Password must consist of 6-18 latin symbols, numbers or \'-\' and \'_\' symbols';
    //     }

    //     $data['old_password'] = $this->request->getPut('old_password');
    //     if ((!is_null($data['old_password'])) && (!is_string($data['old_password']))) 
    //     {
    //         $errors['old_password'] = 'Old password must be a string';
    //     }

    //     $data['first_name'] = $this->request->getPut('first_name');
    //     if ((!is_null($data['first_name'])) && (!is_string($data['first_name']))) 
    //     {
    //         $errors['first_name'] = 'String expected';
    //     }

    //     $data['last_name'] = $this->request->getPut('last_name');
    //     if ((!is_null($data['last_name'])) && (!is_string($data['last_name']))) 
    //     {
    //         $errors['last_name'] = 'String expected';
    //     }

    //     if (!ctype_digit($userId) || ($userId < 0)) 
    //     {
    //         $errors['id'] = 'Id must be a positive integer';
    //     }

    //     $data['id'] = (int)$userId;

    //     if ($errors) 
    //     {
    //         $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
    //         throw $exception->addErrorDetails($errors);
    //     }

    //     try 
    //     {
    //         $this->usersService->updateUser($data);
    //     } 
    //     catch (ServiceException $e) 
    //     {
    //         switch ($e->getCode()) 
    //         {
    //             case UsersService::ERROR_UNABLE_UPDATE_USER:
    //             case UsersService::ERROR_USER_NOT_FOUND:
    //                 throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
    //             default:
    //                 throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
    //         }
    //     }
    // }

    /**
     * DELETE AN EXISTING USER
     *
     * @param string $userId
     */
    public function deleteProjectAction($projectId)
    {
        if (!ctype_digit($projectId) || ($projectId < 0)) 
        {
            $errors['projectId'] = 'Id must be a positive integer';
        }

        try 
        {
            $this->projectsService->deleteProject((int)$projectId);
        } 
        catch (ServiceException $e) 
        {
            switch ($e->getCode()) 
            {
                case ProjectsService::ERROR_UNABLE_DELETE_PROJECT :
                case ProjectsService::ERROR_PROJECT_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }
}
