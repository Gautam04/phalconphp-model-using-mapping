<?php
namespace App\Models;
class UserProject extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $project_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("topic");
        $this->setSource("user_project");
        $this->belongsTo('project_id', 'App\Models\Project', 'id', ['alias' => 'Project']);
        $this->belongsTo('user_id', 'App\Models\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_project';
    }
}
