<?php
namespace App\Models;
class Project extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $p_name;

    /**
     *
     * @var string
     */
    public $p_desc;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("topic");
        $this->setSource("project");
        $this->hasMany('id', 'App\Models\UserProject', 'project_id', ['alias' => 'UserProject']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'project';
    }
}
