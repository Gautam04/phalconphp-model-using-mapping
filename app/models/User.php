<?php
namespace App\Models;
class User extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $address_id;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var string
     */
    public $pass;

    /**
     *
     * @var integer
     */
    public $login;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("topic");
        $this->setSource("user");
        $this->hasMany('id', 'App\Models\Department', 'user_id', ['alias' => 'Department']);
        $this->belongsTo('address_id', 'App\Models\Address', 'id', ['alias' => 'Address']);
        $this->hasManyToMany('id','App\Models\UserProject','user_id','project_id','App\Models\Project','id',['alias' => 'Project']);
    }

    public function getSource()
    {
        return 'user';
    }
}
