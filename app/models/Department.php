<?php
namespace App\Models;
class Department extends \Phalcon\Mvc\Model
{
    public $dept_id;
    public $user_id;
    public $d_name;
    public $d_details;
    public function initialize()
    {
        $this->setSchema("topic");
        $this->setSource("department");
        $this->belongsTo('user_id', 'App\Models\User', 'id', ['alias' => 'User']);
    }
    public function getSource()
    {
        return 'department';
    }
}
