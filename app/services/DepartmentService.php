<?php
namespace App\Services;
use App\Models\Department;
use \Phalcon\Mvc\Model\Query\Builder;

class DepartmentService extends AbstractService
{
	/** UNABLE TO CREATE USER */
	const ERROR_UNABLE_CREATE_DEPARTMENT = 11001;

	/** USER NOT FOUND */
	const ERROR_DEPARTMENT_NOT_FOUND = 11002;

	/** NO SUCH USER */
	const ERROR_INCORRECT_DEPARTMENT = 11003;

	/** UNABLE TO UPDATE USER */
	const ERROR_UNABLE_UPDATE_DEPARTMENT = 11004;

	/** UNABLE TO DELETE USER */
	const ERROR_UNABLE_DELETE_DEPARTMENT = 1105;


	public function createDept(array $deptData)
	{
		try 
		{
			$dept   = new Department();
			$result = $dept->setUserid($deptData['user_id'])
			               ->setDeptName($deptData['d_name'])
			               ->setDeptDetails($deptData['d_details'])
			               ->create();

			if (!$result) 
			{
				throw new ServiceException('Unable to create department', self::ERROR_UNABLE_CREATE_DEPARTMENT);
			}

		} 
		catch (\PDOException $e) 
		{
			if ($e->getCode() == 23505) 
			{
				throw new ServiceException('department already exists', self::ERROR_ALREADY_EXISTS, $e);
			} 
			else 
			{
				throw new ServiceException($e->getMessage(), $e->getCode(), $e);
			}
		}
	}
	
	public function updateDept(array $deptData)
	{
		try 
		{
			$dept = Department::findFirst ( 
				[
					'conditions' => 'id = :id:',
					'bind'       => 
					[
						'id' => $deptData['id']
					]
				]);

			$deptData['user_id']      = (is_null($deptData['user_id'])) ? $dept->getUserId() : $deptData['user_id'];
			$deptData['d_name'] = (is_null($deptData['d_name'])) ? $dept->getDeptName() : $deptData['d_name'];
			$deptData['d_details']  = (is_null($deptData['d_details'])) ? $dept->getDeptDetails() : $deptData['d_details'];

            $result = $dept ->setUserid($deptData['user_id'])
                            ->setDeptName($deptData['d_name'])
                            ->setDeptDetails($deptData['d_details'])
                            ->update();

			if (!$result) 
			{
				throw new ServiceException('Unable to update department', self::ERROR_UNABLE_UPDATE_USER);
			}

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function deleteDept($deptId)
	{
		try 
		{
			$dept = Department::findFirst (
				[
					'conditions' => 'id = :id:',
					'bind'       => 
					[
						'id' => $deptId
					]
				]);

			if (!$dept) 
			{
				throw new ServiceException("department not found", self::ERROR_DEPARTMENT_NOT_FOUND);
			}

			$result = $dept->delete();

			if (!$result) 
			{
				throw new ServiceException('Unable to delete department', self::ERROR_UNABLE_DELETE_DEPARTMENT);
			}

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	// public function getDeptList()
	// {
	// 	try 
	// 	{
	// 		$dept = (new Builder())
    // 					->addFrom("App\Models\Department",'department')
    // 					->join("App\Models\User",'user.id=department.user_id','user',"LEFT")
   	// 					->columns(
	// 						[
    //                             'dept_id'    => 'department.dept_id',
	// 							'd_user_id'  => 'department.user_id',
	// 							'd_name'  	 => 'department.d_name',
	// 							'd_details'  => 'department.d_details',
	// 							'user_id'    => 'user.id',
	// 							'first_name' => 'user.first_name',
	// 							'last_name'  => 'user.last_name',
	// 							'login'      => 'user.login',
	// 						]
	// 					)
	// 					->orderBy('user.last_name, user.first_name, department.d_name')
	// 					->getQuery()
	// 					->execute();

	// 		return $dept->toArray();

	// 	} 
	// 	catch (\PDOException $e) 
	// 	{
	// 		throw new ServiceException($e->getMessage(), $e->getCode(), $e);
	// 	}
	// }

	public function getSingleDept($deptId)
	{
		try 
		{
			$dept = Department::findFirst(
				[
					'conditions' => 'id = :id:',
					'bind'=> ['id' => $deptId],
				]
				);
		
			if (!$dept) 
			{
				return [];
			}
			return $dept->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}
}
