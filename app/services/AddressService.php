<?php
namespace App\Services;

use \Phalcon\Mvc\Model\Query\Builder;
use App\Models\Address;

class AddressService extends AbstractService
{
	/** UNABLE TO CREATE ADDRESS */
	const ERROR_UNABLE_CREATE_ADDRESS = 11001;

	/** ADDRESS NOT FOUND */
	const ERROR_ADDRESS_NOT_FOUND = 11002;

	/** NO SUCH ADDRESS */
	const ERROR_INCORRECT_ADDRESS = 11003;

	/** UNABLE TO UPDATE ADDRESS */
	const ERROR_UNABLE_UPDATE_ADDRESS = 11004;

	/** UNABLE TO DELETE ADDRESS */
	const ERROR_UNABLE_DELETE_ADDRESS = 1105;

	/**
	 * CREATING A NEW ADDRESS
	 *
	 * @param array $addressData
	 */
	public function createAddress(array $addressData)
	{
		try 
		{
			$add   = new Address();
			$result = $add->setStreet($addressData['street'])
                           ->setCity($addressData['city'])
			               ->setState($addressData['state'])
			               ->setCountry($addressData['country'])
			               ->create();

			if (!$result) 
			{
				throw new ServiceException('Unable to create Address', self::ERROR_UNABLE_CREATE_ADDRESS);
			}

		} 
		catch (\PDOException $e) 
		{
			if ($e->getCode() == 23505) 
			{
				throw new ServiceException('Address already exists', self::ERROR_ALREADY_EXISTS, $e);
			} 
			else 
			{
				throw new ServiceException($e->getMessage(), $e->getCode(), $e);
			}
		}
	}
	
	public function updateAddress(array $addressData)
	{
		try 
		{
			$add = Address::findFirst ( 
				[
					'conditions' => 'id = :id:',
					'bind'       => 
					[
						'id' => $addressData['id']
					]
				]);

			$addressData['street']     =    (is_null($addressData['street'])) ? $add->getStreet() : $addressData['street'];
			$addressData['city']       =    (is_null($addressData['city'])) ? $add->getCity() : ($addressData['city']);
			$addressData['state']      =    (is_null($addressData['state'])) ? $add->getSate() : $addressData['state'];
			$addressData['country']    =    (is_null($addressData['country'])) ? $add->getCountry() : $addressData['country'];
			
			$result = $add->setStreet($addressData['street'])
                           ->setCity($addressData['city'])
			               ->setState($addressData['state'])
			               ->setCountry($addressData['country'])
			               ->update();

			if (!$result) 
			{
				throw new ServiceException('Unable to update Address', self::ERROR_UNABLE_UPDATE_ADDRESS);
			}

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function deleteAddress($addId)
	{
		try 
		{
			$add = Address::findFirst (
				[
					'conditions' => 'id = :id:',
					'bind'       => 
					[
						'id' => $addId
					]
				]);

			if (!$add) 
			{
				throw new ServiceException("Address not found", self::ERROR_ADDRESS_NOT_FOUND);
			}

			$result = $add->delete();

			if (!$result) 
			{
				throw new ServiceException('Unable to delete Address', self::ERROR_UNABLE_DELETE_ADDRESS);
			}

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function getAddressList()
	{
		try 
		{
			$add = Address::find (
				[
					'conditions' => '',
					'bind'       => [],
				]
			);

			if (!$add) 
			{
				return [];
			}

			return $add->toArray();
		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function getSingleAddress($addId)
	{
		try 
		{
			$add = Address::findFirst(
				[
					'conditions' => 'id = :id:',
					'bind'=> ['id' => $addId],
				]
				);
		
			if (!$add) 
			{
				return [];
			}
			return $add->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
    }
    

 
}
