<?php
namespace App\Services;
use App\Models\User;
use App\Models\Address;
use App\Models\Department;
use App\Models\Project;
use \Phalcon\Mvc\Model\Query\Builder;

class UsersService extends AbstractService
{
	/** UNABLE TO CREATE USER */
	const ERROR_UNABLE_CREATE_USER = 11001;

	/** USER NOT FOUND */
	const ERROR_USER_NOT_FOUND = 11002;

	/** NO SUCH USER */
	const ERROR_INCORRECT_USER = 11003;

	/** UNABLE TO UPDATE USER */
	const ERROR_UNABLE_UPDATE_USER = 11004;

	/** UNABLE TO DELETE USER */
	const ERROR_UNABLE_DELETE_USER = 1105;


	public function createUser(array $userData)
	{
		try 
		{
			$add = new Address();

			$add->street   = $userData['street'];
			$add->state   = $userData['state'];
			$add->country   = $userData['country'];
			$add->city   = $userData['city'];

			$user = new User();
			$user->first_name = $userData['first_name'];
			$user->last_name = $userData['last_name'];
			$user->pass = $userData['password'];
			$user->login = $userData['login'];
			$user->address = $add;

			$projects = [];
			forEach($userData['projects'] as $p)
			{
				$project = new Project();
				$project->p_name=$p['p_name'];
				$project->p_desc=$p['p_desc'];
				
				array_push($projects,$project);
			}
			$user->project = $projects;
			

			$depts = [];
			forEach($userData['depts'] as $d)
			{
				$dept = new Department();
				$dept->d_name=$d['d_name'];
				$dept->d_details=$d['d_details'];
				array_push($depts,$dept);
			}
			$user->department = $depts;
			$user->save(); 


		} 
		catch (\PDOException $e) 
		{
			if ($e->getCode() == 23505) 
			{
				throw new ServiceException('User already exists', self::ERROR_ALREADY_EXISTS, $e);
			} 
			else 
			{
				throw new ServiceException($e->getMessage(), $e->getCode(), $e);
			}
		}
	}
	
	public function updateUser(array $userData)
	{
		try 
		{
			$user = User::findFirst ( 
				[
					'conditions' => 'id = :id:',
					'bind'       => 
					[
						'id' => $userData['id']
					]
				]);

				$user->getAddress()->update(
					$userData = [
						'street'      => 'BTM',
						'city' => 'Banga',
						'state' => 'Karanat'
					]);

					$userData = [
						'd_name' =>$userData['d_name'],
						'd_details' => $userData['d_details']
					];
				$user->getDepartment()->update($userData,
				function ($dept) {
					if ($dept->dept_id === '71') {
						return true;
					}
			
					return false;
				}
				);

				$userData = [
					'p_name' => 'Wealthness',
					'p_desc' => 'Tested'
				];
			$user->getProject()->update($userData,
			function ($project) {
				if ($project->p_name === 'HDFND') {
					return true;
				}
		
				return false;
			});
					
			if (!$user) 
			{
				return [];
			}
			return $user->toArray();
			
		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function deleteUser($userId)
	{
		try 
		{
			$user = User::findFirst(
				[
					'conditions' => 'id = :id:',
					'bind'=> ['id' => $userId],
				]
				);
			
			$user->getAddress()->delete();
			
			$user->getDepartment()->delete(
				function ($dept) {
					if ($dept->d_name == 'HR') {
						return true;
					}
			
					return false;
				}
			);

			$user->getProject()->delete(
				function ($dept) {
					if ($dept->id == 14) {
						return true;
					}
			
					return false;
				}
			);
			
		
			if (!$user) 
			{
				return [];
			}
			return $user->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function getUserList()
	{
		try 
		{
			$users = (new Builder())
    					->addFrom("App\Models\User",'user')
    					->join("App\Models\Address",'address.id=user.address_id','address',"LEFT")
   						->columns(
							[
								'user_id'    => 'user.id',
								'first_name' => 'user.first_name',
								'last_name'  => 'user.last_name',
								'login'      => 'user.login',
								'address_id' => 'user.address_id',
								'addid'      => 'address.id',
								'city'  	 => 'address.city',
								'street'     => 'address.street',
								'country'    => 'address.country',
								'state'      => 'address.state',
							]
						)
						->orderBy('user.last_name, user.first_name, address.city')
						->getQuery()
						->execute();

			return $users->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function getSingleUser($userId)
	{
		try 
		{
			$user = User::findFirst(
				[
					'conditions' => 'id = :id:',
					'bind'=> ['id' => $userId],
				]
				);
		
			if (!$user) 
			{
				return [];
			}
			return $user->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	public function getDeptList()
	{
		try 
		{
			$dept = (new Builder())
    					->addFrom("App\Models\User",'user')
    					->join("App\Models\Department",'department.user_id=user.id','department',"LEFT")
   						->columns(
							[
								'user_id'    => 'user.id',
								'first_name' => 'user.first_name',
								'last_name'  => 'user.last_name',
								'login'      => 'user.login',
                                'dept_id'    => 'department.dept_id',
								'd_user_id'  => 'department.user_id',
								'd_name'  	 => 'department.d_name',
								'd_details'  => 'department.d_details',
							]
						)
						->orderBy('user.last_name, user.first_name, department.d_name')
						->getQuery()
						->execute();

			return $dept->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}
	public function getProjectList()
	{
		try 
		{
			$user = (new Builder())
    					->addFrom("App\Models\User",'user')
    					->join("App\Models\UserProject",'user.id=user_project.user_id','user_project',"LEFT")
    					->join("App\Models\Project",'user_project.project_id = project.id','project',"LEFT")
   						->columns(
							[
								'user_id'    => 'user.id',
								'first_name' => 'user.first_name',
								'last_name'  => 'user.last_name',
								'login'      => 'user.login',
                                'p_id'    	 => 'project.id',
								'p_name'  	 => 'project.p_name',
								'p_desc'  	 => 'project.p_desc',
							]
						)
						->getQuery()
						->execute();

			return $user->toArray();

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}
}
