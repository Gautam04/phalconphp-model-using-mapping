<?php
namespace App\Services;
use App\Models\User;
use App\Models\Project;

/**
 * BUSINESS-LOGIC FOR USERS
 *
 * Class UsersService
 */
class ProjectsService extends AbstractService
{
	/** UNABLE TO CREATE USER */
	const ERROR_UNABLE_CREATE_PROJECT = 11001;

	/** USER NOT FOUND */
	const ERROR_PROJECT_NOT_FOUND = 11002;

	/** NO SUCH USER */
	const ERROR_INCORRECT_PROJECT = 11003;

	/** UNABLE TO UPDATE USER */
	const ERROR_UNABLE_UPDATE_PROJECT = 11004;

	/** UNABLE TO DELETE USER */
	const ERROR_UNABLE_DELETE_PROJECT = 1105;

	/**
	 * CREATING A NEW USER
	 *
	 * @param array $userData
	 */
	public function createProject(array $projectData)
	{
		try 
		{
			$project = new Project();
			$project->p_name=$projectData['p_name'];
			$project->p_desc=$projectData['p_desc'];
			
			$users = [];
			forEach($projectData['users'] as $p)
			{
				$user = new User();
				$user->first_name = $p['first_name'];
				$user->last_name = $p['last_name'];
				$user->pass = $p['password'];
				$user->login = $p['login'];

				array_push($users,$user);
			}

			$project->user = $users;

			$project->save();


            return $result;

		} 
		catch (\PDOException $e) 
		{
			if ($e->getCode() == 23505) 
			{
				throw new ServiceException('Project already exists', self::ERROR_ALREADY_EXISTS, $e);
			} 
			else 
			{
				throw new ServiceException($e->getMessage(), $e->getCode(), $e);
			}
		}
	}

		/**
	 * DELETE AN EXISTING USER
	 *
	 * @param int $userId
	 */
	public function deleteProject($projectId)
	{
		try 
		{
			$project = Project::findFirst (
				[
					'conditions' => 'id = :id:',
					'bind'       => 
					[
						'id' => $projectId
					]
				]);

			if (!$project) 
			{
				throw new ServiceException("User not found", self::ERROR_PROJECT_NOT_FOUND);
			}

			$result = $project->delete();

			if (!$result) 
			{
				throw new ServiceException('Unable to delete user', self::ERROR_UNABLE_DELETE_PROJECT);
			}

		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * RETURNS USER LIST
	 *
	 * @return array
	 */
	public function getProjectList()
	{
		try 
		{
			$project = Project::find (
				[
					'conditions' => '',
					'bind'       => [],
					'columns'    => "id, p_name, p_desc, user_id",
				]
			);

			if (!$project) 
			{
				return [];
			}

			return $project->toArray();
			
		} 
		catch (\PDOException $e) 
		{
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	// public function getSingleUser($userId)
	// {
	// 	try 
	// 	{
	// 		$user = Users::findFirst(
	// 			[
	// 				'conditions' => 'id = :id:',
	// 				'bind'       => ['id' => $userId],
	// 				'columns'    => "id, first_name, last_name, login",
	// 			]
	// 			);

	// 		if (!$user) 
	// 		{
	// 			return [];
	// 		}

	// 		return $user->toArray();

	// 	} 
	// 	catch (\PDOException $e) 
	// 	{
	// 		throw new ServiceException($e->getMessage(), $e->getCode(), $e);
	// 	}
	// }

}
