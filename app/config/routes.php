<?php
//USER TABLE API CRUD METHODS 
$usersCollection = new Phalcon\Mvc\Micro\Collection();
$usersCollection->setHandler('\App\Controllers\UsersController', true);
$usersCollection->setPrefix('/user');
$usersCollection->post('/add', 'addAction');
$usersCollection->get('/test', 'testAction');
$usersCollection->get('/list', 'getUserListAction');
$usersCollection->get('/dept', 'getUserDepartmentListAction');
$usersCollection->get('/project', 'getUserProjectListAction');
$usersCollection->get('/data/{userId:[1-9][0-9]*}', 'getSingleUserListAction');
$usersCollection->get('/userProject/{userId:[1-9][0-9]*}', 'getSingleUserProjectListAction');
$usersCollection->put('/{userId:[1-9][0-9]*}', 'updateUserAction');
$usersCollection->delete('/{userId:[1-9][0-9]*}', 'deleteUserAction');
$app->mount($usersCollection);

// NOT FOUND URLS
$app->notFound (function () use ($app) 
{
      $exception =
		new \App\Controllers\HttpExceptions\Http404Exception 
		(
          _('URI not found or error in request.'),
          \App\Controllers\AbstractController::ERROR_NOT_FOUND,
          new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
        );
      throw $exception;
});

//PROJECT TABLE API CRUD METHODS 
$projectsCollection = new Phalcon\Mvc\Micro\Collection();
$projectsCollection->setHandler('\App\Controllers\ProjectsController', true);
$projectsCollection->setPrefix('/project');
$projectsCollection->post('/add', 'addProjectAction');
$projectsCollection->get('/list', 'getProjectListAction');
$projectsCollection->delete('/{projectId:[1-9][0-9]*}', 'deleteProjectAction');
$app->mount($projectsCollection);

// NOT FOUND URLS
$app->notFound (function () use ($app) 
{
      $exception =
		new \App\Controllers\HttpExceptions\Http404Exception 
		(
          _('URI not found or error in request.'),
          \App\Controllers\AbstractController::ERROR_NOT_FOUND,
          new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
        );
      throw $exception;
});

//ADDRESS TABLE API CRUD METHODS 
$addressCollection = new Phalcon\Mvc\Micro\Collection();
$addressCollection->setHandler('\App\Controllers\AddressController', true);
$addressCollection->setPrefix('/address');
$addressCollection->post('/add', 'addAddressAction');
$addressCollection->get('/list', 'getAddressListAction');
$addressCollection->get('/single/{userId:[1-9][0-9]*}', 'getSingleAddressAction');
$addressCollection->put('/{userId:[1-9][0-9]*}', 'updateAddressAction');
$addressCollection->delete('/{projectId:[1-9][0-9]*}', 'deleteAddressAction');
$app->mount($addressCollection);

// NOT FOUND URLS
$app->notFound (function () use ($app) 
{
      $exception =
		new \App\Controllers\HttpExceptions\Http404Exception 
		(
          _('URI not found or error in request.'),
          \App\Controllers\AbstractController::ERROR_NOT_FOUND,
          new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
        );
      throw $exception;
});

//DEPARTMENT TABLE API CRUD METHODS 
$departmentCollection = new Phalcon\Mvc\Micro\Collection();
$departmentCollection->setHandler('\App\Controllers\DepartmentController', true);
$departmentCollection->setPrefix('/department');
$departmentCollection->post('/add', 'addDeptAction');
$departmentCollection->get('/list', 'getDeptListAction');
$departmentCollection->get('/single/{userId:[1-9][0-9]*}', 'getSingleDeptListAction');
$departmentCollection->put('/{userId:[1-9][0-9]*}', 'updateDeptAction');
$departmentCollection->delete('/{projectId:[1-9][0-9]*}', 'deleteDeptAction');
$app->mount($departmentCollection);

// NOT FOUND URLS
$app->notFound (function () use ($app) 
{
      $exception =
		new \App\Controllers\HttpExceptions\Http404Exception 
		(
          _('URI not found or error in request.'),
          \App\Controllers\AbstractController::ERROR_NOT_FOUND,
          new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
        );
      throw $exception;
});
