<?php

// SET APPLICATION LEVEL CONFIGURATIONS HERE.
return new \Phalcon\Config 
(
    [
        'database' => 
        [
            'adapter' => 'Mysql',
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'topic',
        ],

        'application' => 
        [
	        'controllersDir' => "app/controllers/",
	        'modelsDir'      => "app/models/",
            'baseUri'        => "/api",
            'logPath'        => "app/logs/app.log",
        ],
    ]
);
